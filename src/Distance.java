/**
 * Jared Kirk
 * Delaney Giacalone
 */
public class Distance {
    public Cluster cluster1, cluster2;
    public int cluster1Index, cluster2Index;
    public double distance;

    public Distance(Cluster cluster1, Cluster cluster2, int cluster1Index, int cluster2Index) {
        this.cluster1 = cluster1;
        this.cluster2 = cluster2;
        this.cluster1Index = cluster1Index;
        this.cluster2Index = cluster2Index;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getDistance() {
        return distance;
    }
}
