import java.util.ArrayList;
import java.util.Arrays;

/**
 * Jared Kirk
 * Delaney Giacalone
 */
public class KMeansAlgorithm {
    public ArrayList<ArrayList<Double>> dataSet = new ArrayList<>();
    public ArrayList<ArrayList<Double>> centroids;
    public int clusters;
    int stopCondition = 10, iterations = 0;
    boolean continueKMeans = false;

    /**
     * k means algorithm for integer points.
     * @param dataSet The dataset.
     * @param clusters The number of clusters to use.
     */
    public void kmeansInt(ArrayList<ArrayList<Double>> dataSet, int clusters) {
        this.dataSet = dataSet;
        this.clusters = clusters;
        centroids = selectInitialCentroidsInt(clusters);
        ArrayList<ArrayList<Integer>> cl = new ArrayList<>(); //Actual Clusters
        ArrayList<int[]> oldS = new ArrayList<int[]>(); //Old s vectors
        ArrayList<ArrayList<Double>> oldCentroids = new ArrayList<>();

        do {
            ArrayList<int[]> s = new ArrayList<int[]>(); //Vectors of size D
            int num[] = new int[dataSet.size()]; //Will be num points in each cluster
            cl = new ArrayList<>(); //Actual Clusters

            //Initialize everything.
            for (int j = 0; j < clusters; j++) {
                s.add(new int[dataSet.size()]);
                num[j] = 0;
                cl.add(new ArrayList<Integer>());
            }

            //For each point x in the dataset
            for(int i = 0; i < dataSet.size(); i++) {
                //1.Assign the point x to the closest cluster.
                int cluster = findClosestCluster(dataSet.get(i));
                //2.Add point x to the cl array of cluster points (This adds the index)
                cl.get(cluster).add(i);
                //3.s[j] = s[j] + x, where j is the cluster chosen I believe
                s.get(cluster)[i] = 1; //Flag to set index of point.
                //4.num[j] = num[j] + 1
                num[cluster]++;
            }

            //Reassign the centroids.
            for(int j = 0; j < clusters; j++) {
                ArrayList<Double> numerator = new ArrayList<>();
                for(int i = 0; i < dataSet.get(j).size(); i++) {
                    numerator.add(0.0); //0.0 0.0 aka number of columns
                }
                //m[j] := s[j]/num[j]
                //Create new points that don't exist yet. The center of the cluster.
                for(int test : cl.get(j)) {
                    for(int i = 0; i < dataSet.get(test).size(); i++) {
                        numerator.set(i,numerator.get(i) + (double)dataSet.get(test).get(i));
                    }
                }
                for(int i = 0; i < numerator.size(); i++) {
                    numerator.set(i, numerator.get(i) / (double)num[j]);
                }
                ArrayList<Double> newCentroid = new ArrayList<>();
                for(double d : numerator) {
                    newCentroid.add(d);
                }
                centroids.set(j, newCentroid);
                //System.out.println("New centroid: " + newCentroid);
            }
            //Check stopping conditions
            /*continueKMeans = checkPointReassignment(oldS, s);
            continueKMeans = checkCentroidReassignment(oldCentroids);
            oldS = s;
            oldCentroids = centroids;
            iterations++;*/
        } while(isStoppingCondition());

        //Output
        output(cl);
    }

    /**
     * Takes a point and finds the centroid with the minimum distance. Uses Euclidean distance.
     * @param point
     * @return The centroid with the minimum distance.
     */
    public int findClosestCluster(ArrayList<Double> point) {
        int closestCentroid = 0;
        double minCentroidDistance = 0;

        for(int i = 0; i < centroids.size(); i++) {
            double euclideanDistance = 0;
            for(int k = 0; k < point.size(); k++) {
                euclideanDistance += Math.pow((point.get(k) - (double)centroids.get(i).get(k)), 2);
            }
            euclideanDistance = Math.sqrt(euclideanDistance);
            if(euclideanDistance < minCentroidDistance || i == 0) {
                minCentroidDistance = euclideanDistance;
                closestCentroid = i;
            }
        }
        return closestCentroid;
    }

    public boolean checkCentroidReassignment(ArrayList<ArrayList<Double>> oldCentroids) {
        for(int i = 0; i < oldCentroids.size(); i++) {
            for(int k = 0; k < oldCentroids.get(i).size(); k++) {
                if(oldCentroids.get(i).get(k) != centroids.get(i).get(k)) {
                    System.out.println("keep going centroid");
                    return true;
                }
            }
        }
        System.out.println("exit centroid");
        return false;
    }

    /**
     * Checks if there has been a reassignment of points.
     * @param oldS
     * @param s
     * @return
     */
    public boolean checkPointReassignment(ArrayList<int[]> oldS, ArrayList<int[]> s) {
        for(int i = 0; i < oldS.size(); i++) {
            if(!Arrays.equals(oldS.get(i), s.get(i))) {
                System.out.println("keep going point");
                return true;

            }
        }
        System.out.println("exit point");
        return false;
    }

    public boolean isStoppingCondition() {
        stopCondition--;
        if(stopCondition > 0)
            return true;
        else return false;
    }

    /**
     * Selects the initial k cluster points. Does a random 4 integers.
     * @param k Chosen by the arguments to the program.
     * @return
     */
    public ArrayList<ArrayList<Double>> selectInitialCentroidsInt(int k) {
        ArrayList<ArrayList<Double>> centroids = new ArrayList<>();
        ArrayList<Integer> randomNums = new ArrayList<>();
        int assignments = 0;
        //Loop through k many times getting random numbers.
        while(assignments < k) {
            int random = (int)(Math.random() * (dataSet.size()));
            //If the random number hasn't been chosen yet, add the corresponding cluster point.
            if(!randomNums.contains(random)) {
                randomNums.add(random);
                centroids.add(dataSet.get(random));
                assignments++;
            }
        }

        return centroids;
    }

    public void output(ArrayList<ArrayList<Integer>> cl) {
        //Loop through k centroids
        for(int k = 0; k < centroids.size(); k++) {
            double avgDistance = 0.0, minDistance = (dataSet.get(cl.get(k).get(0)).get(0)), maxDistance = 0.0, euclideanDistance = 0.0;
            String points = "";
            //Loop through the indices of the dataset in this cluster k
            for(int i = 0; i < cl.get(k).size(); i++) {
                points += dataSet.get(cl.get(k).get(i)) + "\n";

                //Get the point in the dataset and find the distance to the center
                for(int l = 0; l < dataSet.get(cl.get(k).get(i)).size(); l++) {
                    euclideanDistance += Math.pow((dataSet.get(cl.get(k).get(i)).get(l) -
                            (double)centroids.get(k).get(l)), 2);
                }
                euclideanDistance = Math.sqrt(euclideanDistance);
                if(euclideanDistance > maxDistance || i == 0) {
                    maxDistance = euclideanDistance;
                }
                else if(euclideanDistance < minDistance || i == 0) {
                    minDistance = euclideanDistance;
                }
                avgDistance += euclideanDistance;
            }
            avgDistance /= cl.get(k).size();
            String regex = "([.])([0][' '])"; //matches .0 to get rid of doubles.
            System.out.println("Cluster " + k + ":"
                                + "\nCenter: " + centroids.get(k)
                                + "\nMax Distance to Center: " + maxDistance
                                + "\nMin Distance to Center: " + minDistance
                                + "\nAvg Distance to Center: " + avgDistance
                                + "\n" + cl.get(k).size() + " Points:"
                                + "\n" + points);//.replaceAll("\\[", "").replaceAll("\\]","").replaceAll(regex, ""));
        }
    }
}
