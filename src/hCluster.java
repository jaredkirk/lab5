import java.util.ArrayList;

/**
 * Jared Kirk
 * Delaney Giacalone
 */
public class hCluster {
    public ArrayList<ArrayList<Double>> dataSet;
    ArrayList<ArrayList<Integer>> cl = new ArrayList<>(); //Clusters by indices
    ArrayList<Cluster> clusters = new ArrayList<>();
    public ArrayList<Distance> clusterDistances = new ArrayList<>();
    ArrayList<ArrayList<Double>> cluster1Children = new ArrayList<>();
    ArrayList<ArrayList<Double>> cluster2Children = new ArrayList<>();
    ArrayList<Cluster> thresholdClusters = new ArrayList<>();

    public int threshold;

    public hCluster(ArrayList<ArrayList<Double>> dataSet, int threshold) {
        this.dataSet = dataSet;
        this.threshold = threshold;
    }

    public hCluster(ArrayList<ArrayList<Double>> dataSet) {
        this.dataSet = dataSet;
    }

    public void agglomerative() {
        int l = 0;
        for(ArrayList<Double> point : dataSet) {
            Cluster newCluster = new Cluster(point); //C1i = {xi}
            newCluster.indexInDataset = l;
            clusters.add(newCluster);//C1 := {C11, . . . , C1n};
            l++;
        }
        int i = 0;

        while(clusters.size() > 1) {
            clusterDistances = new ArrayList<>(); //Reset distances between clusters
            for(int j = 0; j < clusters.size(); j++) { //might need to be size() - 1
                for(int k = j + 1; k < clusters.size(); k++) {
                    //Compute distance of two clusters and add it to a list of cluster distances.
                    Distance clusterPair = new Distance(clusters.get(j), clusters.get(k), j, k);
                    clusterPair.setDistance(completeLinkDistance(clusterPair.cluster1, clusterPair.cluster2));
                    clusterDistances.add(clusterPair);
                }
            }

            //Find the shortest distance and merge those clusters.
            Distance minClusters = clusterDistances.get(0);
            double minDistance = clusterDistances.get(0).getDistance();
            for(int k = 0; k < clusterDistances.size(); k++) {
                if(clusterDistances.get(k).getDistance() < minDistance) {
                    minDistance = clusterDistances.get(k).getDistance();
                    minClusters = clusterDistances.get(k);
                }
            }
            //Merge the two clusters.
            int s = minClusters.cluster1Index, r = minClusters.cluster2Index;
            ArrayList<Cluster> nextIterationClusters = new ArrayList<>();
            for(int j = 0; j < clusters.size(); j++) {
                //Add the unaffected cluster to the new list.
                if(j != r && j != s) {
                    nextIterationClusters.add(clusters.get(j));
                }
                //Combine clusters r and s into one single cluster.
                else if(j == r) {
                    Cluster parent = new Cluster();
                    parent.addChildCluster(clusters.get(r));
                    parent.addChildCluster(clusters.get(s));
                    parent.groupName = clusters.get(r).groupName + 10;
                    parent.distance = minDistance;

                    nextIterationClusters.add(parent);
                }
            }
            //Reset the list of clusters to the updated list.
            clusters = nextIterationClusters;
            i++;
        }
    }

    /**
     * Find distance between two clusters. The distance is the two points furthest from each other.
     * @param cluster1
     * @param cluster2
     * @return
     */
    public double completeLinkDistance(Cluster cluster1, Cluster cluster2) {
        cluster1Children = new ArrayList<>();
        cluster2Children = new ArrayList<>();
        getChildren1(cluster1);
        getChildren2(cluster2);

        double maxDistance = 0.00;
        for(int i = 0; i < cluster1Children.size(); i++) { //Loop through the first cluster
            for(int k = 0; k < cluster2Children.size(); k++) { //Loop through the second cluster
                double euclideanDistance = 0.00;
                for(int l = 0; l < cluster1Children.get(i).size(); l++) { //Loop through the x,y, etc. points
                    if(cluster2Children.get(k).size() > 0) {
                        euclideanDistance += Math.pow((cluster1Children.get(i).get(l) -
                                (double) cluster2Children.get(k).get(l)), 2);
                    }
                }
                euclideanDistance = Math.sqrt(euclideanDistance);
                if (euclideanDistance > maxDistance || (i == 0 && k == 0)) {
                    maxDistance = euclideanDistance;
                }
            }
        }

        return maxDistance;
    }

    //Uncomment this for average-link distance
    /*
    public double completeLinkDistance(Cluster cluster1, Cluster cluster2) {
        cluster1Children = new ArrayList<>();
        cluster2Children = new ArrayList<>();
        getChildren1(cluster1);
        getChildren2(cluster2);

        double avg = 0.00;
        int numIterations = 0;
        double maxDistance = 0.00;
        for(int i = 0; i < cluster1Children.size(); i++) { //Loop through the first cluster
            for(int k = 0; k < cluster2Children.size(); k++) { //Loop through the second cluster
                double euclideanDistance = 0.00;
                for(int l = 0; l < cluster1Children.get(i).size(); l++) { //Loop through the x,y, etc. points
                    if(cluster2Children.get(k).size() > 0) {
                        euclideanDistance += Math.pow((cluster1Children.get(i).get(l) -
                                (double) cluster2Children.get(k).get(l)), 2);
                    }
                }
                euclideanDistance = Math.sqrt(euclideanDistance);
                numIterations++;
                avg += euclideanDistance;
                if (euclideanDistance > maxDistance || (i == 0 && k == 0)) {
                    maxDistance = euclideanDistance;
                }
            }
        }
        avg = avg/numIterations;
        //System.out.println("Distance between 2 clusters: " + maxDistance);
        return avg;
    }
     */
    public void getChildren1(Cluster c) {
        if(c.getChildren().size() > 0) {
            //Find all the children clusters, call getChildren on them.
            for(int i = 0; i < c.getChildren().size(); i++) {
                getChildren1(c.getChildren().get(i));
            }
        }
        else cluster1Children.add(c.getPoint());
    }

    public void getChildren2(Cluster c) {
        if(c.getChildren().size() > 0) {
            //Find all the children clusters, call getChildren on them.
            for(int i = 0; i < c.getChildren().size(); i++) {
                getChildren2(c.getChildren().get(i));
            }
        }
        else cluster2Children.add(c.getPoint());
    }

    /**
     * Cuts the dendrogram into smaller clusters based on the threshold.
     * @param cluster Full tree to cut.
     * @param threshold Threshold to cut at.
     */
    public void cutThreshold(ArrayList<Cluster> cluster, int threshold) {
        for(int k = 0; k < cluster.size(); k++) {
            if(cluster.get(k).distance < threshold) {
                //System.out.println("Added distance: " + cluster.get(k).distance);
                thresholdClusters.add(cluster.get(k));
                //return;
            } else {
                cutThreshold(cluster.get(k).getChildren(), threshold);
            }
        }
    }

    /**
     * Loop through the cluster and get the indices in the dataset of the points in the cluster.
     * Proceed to use this for kmeans output.
     * @param cluster
     * @param clPoints
     */
    public void getClusterPoints(Cluster cluster, ArrayList<Integer> clPoints) {
        if(cluster.getChildren().size() < 1) {
            //thresholdClusters.add(cluster.get(k));
            clPoints.add(cluster.indexInDataset);
        } else {
            for(int k = 0; k < cluster.getChildren().size(); k++){
                getClusterPoints(cluster.getChildren().get(k), clPoints);
            }
        }
    }

    public void output(ArrayList<ArrayList<Integer>> cl, int numClusters) {
    //Loop through k centroids
        for(int k = 0; k < numClusters; k++) {
            double avgDistance = 0.0, minDistance = (dataSet.get(cl.get(k).get(0)).get(0)), maxDistance = 0.0, euclideanDistance = 0.0;
            String points = "";
            //Loop through the indices of the dataset in this cluster k
            for(int i = 0; i < cl.get(k).size(); i++) {
                points += dataSet.get(cl.get(k).get(i)) + "\n";

                //Get the point in the dataset and find the distance to the center
                /*for(int l = 0; l < dataSet.get(cl.get(k).get(i)).size(); l++) {
                    euclideanDistance += Math.pow((dataSet.get(cl.get(k).get(i)).get(l) -
                            (double)centroids.get(k).get(l)), 2);
                }*/
                euclideanDistance = Math.sqrt(euclideanDistance);
                if(euclideanDistance > maxDistance || i == 0) {
                    maxDistance = euclideanDistance;
                }
                else if(euclideanDistance < minDistance || i == 0) {
                    minDistance = euclideanDistance;
                }
                avgDistance += euclideanDistance;
            }
            String regex = "([.])([0])"; //matches .0 to get rid of doubles.
            //avgDistance /= cl.get(k).size();
            System.out.println("Cluster " + k + ":"
                    + "\n" + cl.get(k).size() + " Points:"
                    + "\n" + points.replaceAll("\\[", "").replaceAll("\\]","").replaceAll(regex, ""));
        }
    }
}
