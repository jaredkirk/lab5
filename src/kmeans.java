/**
 * Jared Kirk
 * Delaney Giacalone
 */
public class kmeans {
    public static void main(String[] args) {
        String fileName = args[0];
        int k = Integer.parseInt(args[1]);

        Parser parser = new Parser();
        parser.parse(fileName);

        KMeansAlgorithm kmeans = new KMeansAlgorithm();

        kmeans.kmeansInt(parser.dataSet, k);
    }
}
