/**
 * Created by jaredkirk on 11/17/15.
 */
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.*;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
/**
 * Jared Kirk
 * Delaney Giacalone
 */
public class CreateXMLFile {
    private Document doc;

    public void createXmlFile(ArrayList<Cluster> cluster) throws TransformerException {
        DocumentBuilder dBuilder = null;
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try { dBuilder = dbFactory.newDocumentBuilder(); }
        catch (ParserConfigurationException e) { e.printStackTrace(); }
        doc = dBuilder.newDocument();

        //Create Tree name
        Element rootElement = doc.createElement("tree");
        Attr attr = doc.createAttribute("height");
        attr.setValue(cluster.get(0).distance + "");
        rootElement.setAttributeNode(attr);
        doc.appendChild(rootElement);

        //Recursion to create the tree
        appendNode(cluster, rootElement);

        //Write to file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File("output.xml"));
        transformer.transform(source, result);
        // Output to console for testing
        StreamResult consoleResult = new StreamResult(System.out);
        transformer.transform(source, consoleResult);
    }

    public void appendNode(ArrayList<Cluster> cluster, Element parent) {
        Element xmlNode = null;
        Attr attr = null;
        //If the node is a leaf node
        if(cluster.size() == 0) {
            return;
        }
        for(int k = 0; k < cluster.size(); k++) {
            if(cluster.get(k).getChildren().size() == 0) {
                xmlNode = doc.createElement("leaf");
                attr = doc.createAttribute("point");
                attr.setValue(cluster.get(k).getPoint() + "");
                xmlNode.setAttributeNode(attr);
                parent.appendChild(xmlNode);
            } else {
                xmlNode = doc.createElement("node");
                attr = doc.createAttribute("height");
                attr.setValue(cluster.get(k).distance + "");
                xmlNode.setAttributeNode(attr);
                parent.appendChild(xmlNode);
            }
            appendNode(cluster.get(k).getChildren(), xmlNode);
        }
    }
}
