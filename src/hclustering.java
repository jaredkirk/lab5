import javax.xml.transform.TransformerException;
import java.util.ArrayList;

/**
 * Jared Kirk
 * Delaney Giacalone
 */
public class hclustering {
    public static void main(String[] args) throws TransformerException {
        String fileName = args[0];
        int threshold;
        if(args.length > 1)
            threshold = Integer.parseInt(args[1]);
        else
            threshold = Integer.MAX_VALUE;

        Parser parser = new Parser();
        parser.parse(fileName);

        hCluster hcluster = new hCluster(parser.dataSet, threshold);
        hcluster.agglomerative();
        CreateXMLFile xml = new CreateXMLFile();
        xml.createXmlFile(hcluster.clusters);
        hcluster.cutThreshold(hcluster.clusters, threshold);
        //System.out.println("Num of clusters: " + hcluster.thresholdClusters.size());
        //loop through clusters, get children.
        ArrayList<ArrayList<Integer>> cl = new ArrayList<>();
        for(int i = 0; i < hcluster.thresholdClusters.size(); i++) {
            ArrayList<Integer> clPoints = new ArrayList<>();
            hcluster.getClusterPoints(hcluster.thresholdClusters.get(i), clPoints);
            cl.add(clPoints);
        }
        hcluster.output(cl, cl.size());
    }
}
