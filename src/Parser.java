import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Jared Kirk
 * Delaney Giacalone
 */
public class Parser {
    public ArrayList<ArrayList<Double>> dataSet = new ArrayList<>();

    public String[] restrictions;

    /**
     * Parse the csv file.
     *
     * @param csvFile
     */
    public void parse(String csvFile) {
        Scanner scanner = null;
        try {
            File file = new File(csvFile);
            scanner = new Scanner(file);
        }
        catch(IOException e){}

        restrictions = scanner.nextLine().split(",");
        parseData(scanner);
        //print();
    }

    /**
     * Parse the data. Add it to the corresponding ArrayList.
     *
     * @param scanner The scanner that reads the file, with the header lines already read.
     */
    public void parseData(Scanner scanner) {
        while(scanner.hasNext()) {
            String nextLine = scanner.nextLine();
            String lineData[] = nextLine.split(",");

            ArrayList<Double> row = new ArrayList<>();
            for(int i = 0; i < lineData.length; i++) {
                if(restrictions[i].equals("1")) row.add(Double.parseDouble(lineData[i]));
            }
            dataSet.add(row);
        }
    }

    /**
     * Print the dataset for testing purposes.
     */
    public void print() {
        for(int i = 0; i < dataSet.size(); i++) {
            for(Double d : dataSet.get(i)) {
                System.out.print(d + ",");
            }
            System.out.println();
        }
    }
}
