import java.util.ArrayList;

/**
 * Jared Kirk
 * Delaney Giacalone
 */
public class Cluster {
    private ArrayList<Cluster> children = new ArrayList<>();
    private ArrayList<Double> point = new ArrayList<>();
    public int groupName = 0;
    public double distance = 0.00;
    public int indexInDataset = 0;

    public Cluster(ArrayList<Double> point) {
        this.point = point;
    }

    public Cluster() {}

    public void addChildCluster(Cluster c) {
        children.add(c);
    }

    public ArrayList<Cluster> getChildren() {
        return children;
    }

    public ArrayList<Double> getPoint() {
        return point;
    }
}
