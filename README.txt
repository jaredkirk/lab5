Jared Kirk
Delaney Giacalone

The K-Means algorithm and H-Clustering algorithm have been implemented.

To run code:

"javac *.java" on the command line.
java kmeans <file.csv> <num clusters>
java hclustering <file.csv> <threshold>